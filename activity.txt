

Book Ecommerce

	1. Login

		-username
		   - check if username already exist
		   - check if null/empty
		   - check if username contains numbers and symbols
	    	   - check if username is greater than 4 characters

		- password 
		   - check if password is registered to the username
		   - check if null/empty
		   - check if password contains numbers and symbols
	    	   - check if password is above 8 characters
		
		- login button
		   -check if button tranfer to products page
		   -check if button is clickable
		   -check if button is unclickable if username or password is null
		   -check if button is in the right place

		
	2. Commenting	
		-Comment box
		   -check if comment box is null
		   -check if comment box returns string
		   -check if comment box accepts input
		   -check if comment box converts into string
		
		-Post button
		   -check if button able to post inputs
		   -check if button is clickable
		   -check if button is function correctly
		   -check if button is unclickable if comment box is null

		- User
		   -check if user exists
		   -check if user has mobile number
	   	   -check if user has a address
	     	   -check if user is authenticated

		-Item
		   -check if item is availble
		   -check if item exist
		   -check if item has a correct description
		   -check if item has a right price

	3. Ordering
		- Item
		   -check if item is availble
		   -check if item exist
		   -check if item has a correct description
		   -check if item has a right price

		- User
		   -check if user exists
		   -check if user has mobile number
	   	   -check if user has a address
	     	   -check if user is authenticated

	  	- Check out button
	  	   -check if button transfer to check out page
		   -check if button is clickable
		   -check if button is function correctly
		   -check if button is usable

		- Cancel button
		   -check if button transfer to homepage
		   -check if button is clickable
		   -check if button is function correctly
		   -check if button is usable
	
	4. Wishlisted
		- Cancel button
		   -check if button transfer to homepage
		   -check if button is clickable
		   -check if button is function correctly
		   -check if button is usable
		
		- Buy now button
	  	   -check if button transfer to check out page
		   -check if button is clickable
		   -check if button is function correctly
		   -check if button is usable

		- User
		   -check if user exists
		   -check if user has mobile number
	   	   -check if user has a address
	     	   -check if user is authenticated

		- Item
		   -check if item is availble
		   -check if item exist
		   -check if item has a correct description
		   -check if item and price is equal

	5. Checking Out
		- Cancel button
		   -check if button transfer to homepage
		   -check if button is clickable
		   -check if button is function correctly
		   -check if button is usable

		- Item
		   -check if item is availble
		   -check if item exist
		   -check if item has a correct description
		   -check if item and price is equal

		- Check out
		   -check if reciept is send to user email
		   -check if total amount is correct
		   -check if prices is correct
		   -check if user is validated

		- User
		   -check if user exists
		   -check if user has mobile number
	   	   -check if user has a address
	     	   -check if user is authenticated
	

